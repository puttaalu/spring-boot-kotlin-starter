package com.example.springstarter.services

import org.junit.Assert
import org.junit.Before
import org.junit.BeforeClass
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.dao.DuplicateKeyException
import org.springframework.data.relational.core.conversion.DbActionExecutionException
import org.springframework.test.context.jdbc.Sql
import java.util.*
import org.junit.jupiter.api.BeforeAll
import org.postgresql.jdbc.EscapedFunctions
import org.springframework.core.io.ClassPathResource
import org.springframework.jdbc.datasource.init.ScriptUtils
import javax.sql.DataSource
import org.postgresql.jdbc.EscapedFunctions.LOG





/**
 *
 * @author aashiks on 13/01/2021
 */


@SpringBootTest
class UserServiceTest : AbstractTest() {

    companion object {
        @BeforeAll
        @JvmStatic
        fun setup(@Autowired dataSource:DataSource) {
            dataSource.connection.use { conn ->
                ScriptUtils.executeSqlScript(conn, ClassPathResource("/data/userServiceTestData.sql"))
            }
        }
    }

    @Autowired
    internal lateinit var userService: UserService


    @Test
    fun shouldCreateUser() {
        var user = userService.createUser("test@example.com", "testPassword", "Test User")
        assert(user != null)
        assert(user.emailAddress != null)
    }

    @Test
    fun shouldNotCreateUser() {
        val exception = Assertions.assertThrows(DbActionExecutionException::class.java) {
            userService.createUser(
                "random@example.com",
                "testPassword",
                "Test User"
            )
        }
        assertEquals( exception.cause?.javaClass,DuplicateKeyException::class.java)

    }

    @Test
    fun shouldGetUser() {
        val user = userService.getUser(UUID.fromString("86dc4b3a-5665-11eb-8828-d7edc924f824"))
        assert(user.isPresent)
        assertEquals(user.get().emailAddress, "random2@example.com")
    }

    @Test
    fun shouldGetUserByEmail() {
        val user = userService.getUserByEmail("random2@example.com")
        assert(user.isPresent)
        assertEquals(user.get().id, UUID.fromString("86dc4b3a-5665-11eb-8828-d7edc924f824"))
    }
}