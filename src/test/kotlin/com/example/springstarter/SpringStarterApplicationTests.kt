package com.example.springstarter

import com.example.springstarter.services.AbstractTest
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer


@SpringBootTest
class SpringStarterApplicationTests : AbstractTest(){

	@Test
	fun contextLoads() {
	}

}
