package com.example.springstarter.services

import org.springframework.test.context.DynamicPropertyRegistry
import org.springframework.test.context.DynamicPropertySource
import org.testcontainers.containers.PostgreSQLContainer

/**
 *
 * @author aashiks on 14/01/2021
 *
 */

class TestPostgresContainer(image: String) : PostgreSQLContainer<TestPostgresContainer>(image)

abstract class AbstractTest {
    companion object {
        private val container = TestPostgresContainer("postgres:13").apply {
            withDatabaseName("testdb")
            withUsername("test")
            withPassword("test")
            start()
        }

        @JvmStatic
        @DynamicPropertySource
        fun properties(registry: DynamicPropertyRegistry) {
            registry.add("spring.datasource.url", container::getJdbcUrl);
            registry.add("spring.datasource.password", container::getPassword);
            registry.add("spring.datasource.username", container::getUsername);
        }
    }
}