package com.example.springstarter

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SpringStarterApplication

fun main(args: Array<String>) {
    runApplication<SpringStarterApplication>(*args)
}
