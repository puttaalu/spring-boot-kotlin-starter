package com.example.springstarter.services

import com.example.springstarter.models.KUserDetails
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service

/**
 *
 * @author aashiks on 14/01/2021
 *
 */

@Service("KUserDetailsService")
class KUserDetailsService : UserDetailsService {

    @Autowired
    internal lateinit var userService: UserService

    /**
     * Locates the user based on the username. In the actual implementation, the search
     * may possibly be case sensitive, or case insensitive depending on how the
     * implementation instance is configured. In this case, the `UserDetails`
     * object that comes back may have a username that is of a different case than what
     * was actually requested..
     * @param username the username identifying the user whose data is required.
     * @return a fully populated user record (never `null`)
     * @throws UsernameNotFoundException if the user could not be found or the user has no
     * GrantedAuthority
     */
    override fun loadUserByUsername(username: String): UserDetails {
        val user = userService.getUserByEmail(username)
            .orElseThrow { UsernameNotFoundException("Username $username is not found") }

        if (user.roles.isEmpty())
            throw UsernameNotFoundException("Username $username is not found")

        return KUserDetails(user)
    }

}