package com.example.springstarter.services

import com.example.springstarter.models.User
import com.example.springstarter.repositories.UserRepository
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.jdbc.core.JdbcAggregateTemplate
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service
import java.util.*

/**
 *
 * @author aashiks on 13/01/2021
 *
 */

@Service
class UserService {
    internal val logger = LoggerFactory.getLogger(this.javaClass)

    @Autowired
    internal lateinit var encoder: PasswordEncoder

    @Autowired
    internal lateinit var userRepository: UserRepository

    @Autowired
    internal lateinit var jdbcTemplate: JdbcAggregateTemplate

    fun createUser(
        emailAddress: String,
        password: String,
        fullName: String
    ): User {
        var user = User()
        user.active = true
        user.fullName = fullName
        user.emailAddress = emailAddress
        user.passwordHash = encoder.encode(password)
        user.id = UUID.randomUUID()
        return jdbcTemplate.insert(user)
    }

    fun getUserByEmail(emailAddress: String): Optional<User> {
        return userRepository.findByEmailAddress(emailAddress)
    }

    fun getUser(id: UUID): Optional<User?> {
        return userRepository.findById(id)
    }

}