package com.example.springstarter.authentication

import org.springframework.security.authentication.AccountExpiredException
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler
import kotlin.Throws
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.core.AuthenticationException
import org.springframework.stereotype.Component
import java.io.IOException

@Component
class KCustomAuthFailureHandler : SimpleUrlAuthenticationFailureHandler() {
    @Throws(IOException::class)
    override fun onAuthenticationFailure(
        req: HttpServletRequest,
        res: HttpServletResponse,
        ex: AuthenticationException
    ) {
        var errorCode = "unknown"
        if (ex is BadCredentialsException) {
            errorCode = "badcredentials"
        } else if (ex is AccountExpiredException) {
            errorCode = "expired"
        }
        redirectStrategy.sendRedirect(req, res, String.format("/login?error=%s", errorCode))
    }
}