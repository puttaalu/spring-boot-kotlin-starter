package com.example.springstarter.authentication

import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler
import org.springframework.security.web.authentication.AuthenticationSuccessHandler
import org.springframework.stereotype.Component

class KCustomAuthSuccessHandler : SimpleUrlAuthenticationSuccessHandler(), AuthenticationSuccessHandler {
    init {
        setUseReferer(true)
    }
}