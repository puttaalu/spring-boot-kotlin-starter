package com.example.springstarter.authentication

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.security.web.AuthenticationEntryPoint
import org.springframework.http.converter.HttpMessageConverter
import kotlin.Throws
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import org.springframework.http.converter.StringHttpMessageConverter
import org.springframework.http.server.ServerHttpResponse
import org.springframework.http.server.ServletServerHttpResponse
import org.springframework.security.core.AuthenticationException
import org.springframework.stereotype.Component
import java.io.IOException

@Component
class KCustomAuthenticationEntryPoint(mapper: ObjectMapper) : AuthenticationEntryPoint {
    private val messageConverter: HttpMessageConverter<String>
    private val mapper: ObjectMapper
    @Throws(IOException::class)
    override fun commence(
        httpServletRequest: HttpServletRequest,
        httpServletResponse: HttpServletResponse,
        e: AuthenticationException
    ) {
        val outputMessage: ServerHttpResponse = ServletServerHttpResponse(httpServletResponse)
        outputMessage.setStatusCode(HttpStatus.UNAUTHORIZED)
        messageConverter.write(mapper.writeValueAsString(e), MediaType.APPLICATION_JSON, outputMessage)
    }

    init {
        messageConverter = StringHttpMessageConverter()
        this.mapper = mapper
    }
}