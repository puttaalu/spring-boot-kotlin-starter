package com.example.springstarter.config

import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.io.ClassPathResource
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.core.userdetails.UserDetailsService
import kotlin.Throws
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer
import org.springframework.security.oauth2.provider.code.AuthorizationCodeServices
import org.springframework.security.oauth2.provider.code.JdbcAuthorizationCodeServices
import org.springframework.security.oauth2.provider.token.TokenStore
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter
import org.springframework.security.oauth2.provider.token.store.KeyStoreKeyFactory
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer
import java.lang.Exception
import javax.sql.DataSource

@Configuration
@EnableAuthorizationServer
class OAuthConfiguration : AuthorizationServerConfigurerAdapter() {
    @Autowired
    @Qualifier("authenticationManagerBean")
    internal lateinit var authManager: AuthenticationManager

    @Autowired
    internal lateinit var  passwordEncoder: PasswordEncoder

    @Autowired
    @Qualifier("KUserDetailsService")
    internal lateinit var  userService: UserDetailsService

    @Autowired
    internal lateinit var  dataSource: DataSource

    @Value("\${jwt.keystore.password}")
    var keystorePassword: String? = null

    @Value("\${jwt.keystore.name}")
    var keystoreName: String? = null

    @Value("\${jwt.keystore.file}")
    var keystoreFile: String? = null

    @Throws(Exception::class)
    override fun configure(clients: ClientDetailsServiceConfigurer) {
        clients.jdbc(dataSource)
            .passwordEncoder(passwordEncoder)
    }

    @Bean
    protected fun authorizationCodeServices(): AuthorizationCodeServices {
        return JdbcAuthorizationCodeServices(dataSource)
    }

    @Bean
    fun tokenStore(): TokenStore {
        return JwtTokenStore(accessTokenConverter())
    }

    @Bean
    protected fun accessTokenConverter(): JwtAccessTokenConverter {
        val keyStoreKeyFactory = KeyStoreKeyFactory(
            ClassPathResource(
                keystoreFile!!
            ), keystorePassword!!.toCharArray()
        )
        val converter = JwtAccessTokenConverter()
        converter.setKeyPair(keyStoreKeyFactory.getKeyPair(keystoreName))
        return converter
    }

    override fun configure(endpoints: AuthorizationServerEndpointsConfigurer) {
        endpoints
            .accessTokenConverter(accessTokenConverter())
            .userDetailsService(userService)
            .authenticationManager(authManager)
    }
}