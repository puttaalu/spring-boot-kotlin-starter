package com.example.springstarter.config

import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry
//oauth|auth
@Configuration
class FrontendConfig : WebMvcConfigurer {
    override fun addViewControllers(registry: ViewControllerRegistry) {
      registry.addViewController("/login").setViewName("forward:/index.html")
    }
}