package com.example.springstarter.config

import com.example.springstarter.authentication.KCustomAuthFailureHandler
import com.example.springstarter.authentication.KCustomAuthSuccessHandler
import com.example.springstarter.authentication.KCustomAuthenticationEntryPoint
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.dao.DaoAuthenticationProvider
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, proxyTargetClass = true)
class ServerSecurityConfig : WebSecurityConfigurerAdapter() {

    @Autowired
    internal lateinit var customAuthenticationEntryPoint: KCustomAuthenticationEntryPoint

    @Autowired
    internal lateinit var authfailureHandler: KCustomAuthFailureHandler

    @Autowired
    @Qualifier("KUserDetailsService")
    internal lateinit var userDetailsService: UserDetailsService

    @Value("\${app.web.logout-success-url}")
    internal lateinit var logoutSuccessUri: String

    @Bean
    fun authenticationProvider(): DaoAuthenticationProvider {
        val provider = DaoAuthenticationProvider()
        provider.setPasswordEncoder(passwordEncoder())
        provider.setUserDetailsService(userDetailsService)
        return provider
    }

    @Bean
    fun passwordEncoder(): PasswordEncoder {
        return BCryptPasswordEncoder()
    }

    @Bean
    @Throws(Exception::class)
    override fun authenticationManagerBean(): AuthenticationManager {
        return super.authenticationManagerBean()
    }

    @Throws(Exception::class)
    override fun configure(http: HttpSecurity) { // @formatter:off
        http.csrf().disable()
        http.httpBasic().disable()
        http
            .formLogin()
            .loginPage("/login")
            .usernameParameter("username")
            .passwordParameter("password")
            .successHandler(KCustomAuthSuccessHandler())
            .failureHandler(authfailureHandler)
            .permitAll()
            .and()
            .logout()
            .deleteCookies()
            .invalidateHttpSession(true)
            .logoutSuccessUrl(logoutSuccessUri)
            .permitAll()
            .and()
            .authorizeRequests()
            .antMatchers(
                "/index.html",// for frontend
                "/login"
            ) // authentication request from. See @ResourceServerConfiguration

            .permitAll()
            .anyRequest().authenticated()
    } // @formatter:on


}