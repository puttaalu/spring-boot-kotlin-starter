package com.example.springstarter.config

import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer
import com.example.springstarter.authentication.KCustomAuthenticationEntryPoint
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer
import kotlin.Throws
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.http.SessionCreationPolicy
import com.example.springstarter.authentication.KCustomAccessDeniedHandler
import org.springframework.context.annotation.Configuration
import java.lang.Exception

@Configuration
@EnableResourceServer
class ResourceServerConfiguration( val customAuthenticationEntryPoint: KCustomAuthenticationEntryPoint) :
    ResourceServerConfigurerAdapter() {
    override fun configure(resources: ResourceServerSecurityConfigurer) {
        resources.resourceId("api")
    }

    @Throws(Exception::class)
    override fun configure(http: HttpSecurity) {
        http
            .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
            .and()
            .antMatcher("/api/**")
            .authorizeRequests()
            .antMatchers("/login").permitAll() // receive the login request here.
            // If we need to add a react application for example, do add the path to assets and all here.
            .antMatchers("/index.html").permitAll() // frontend.
            .antMatchers("/api/users**").hasAuthority("ADMIN")
            .antMatchers("/api/**").authenticated()
            .anyRequest().authenticated()
            .and()
            .exceptionHandling().authenticationEntryPoint(customAuthenticationEntryPoint)
            .accessDeniedHandler(KCustomAccessDeniedHandler())
    }
}