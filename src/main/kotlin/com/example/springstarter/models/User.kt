package com.example.springstarter.models

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.MappedCollection
import org.springframework.data.relational.core.mapping.Table
import java.time.LocalDateTime
import java.util.*

/**
 *
 * @author aashiks on 13/01/2021
 *
 */
@Table("users")
class User {
    @Id
    var id: UUID? = null
    var creationDate: LocalDateTime? = LocalDateTime.now()
    var locked = false
    var active = false
    var emailAddress: String = ""
    var authToken: String? = ""
    var passwordHash: String = ""
    var fullName: String? = ""

    @MappedCollection(idColumn = "user_id", keyColumn = "role_name")
    var roles: Set<Role> = Collections.emptySet()
}