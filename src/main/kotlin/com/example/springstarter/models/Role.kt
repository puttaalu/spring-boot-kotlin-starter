package com.example.springstarter.models

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Table
import java.util.*


/**
 *
 * @author aashiks on 14/01/2021
 *
 */
enum class ROLE {
    USER,
    ADMIN,
    SUPER_ADMIN,
    READ_ONLY_USER
}

@Table("roles")
data class Role(@Id val userId: UUID, val roleName: ROLE)