package com.example.springstarter.repositories

import com.example.springstarter.models.User
import org.springframework.data.jdbc.repository.query.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import java.util.*


/**
 *
 * @author aashiks on 13/01/2021
 *
 */
@Repository
interface UserRepository : CrudRepository<User?, UUID?> {
    @Query("select * from users where email_address = :emailAddress")
    fun findByEmailAddress(@Param("emailAddress") firstName: String?): Optional<User>
}
