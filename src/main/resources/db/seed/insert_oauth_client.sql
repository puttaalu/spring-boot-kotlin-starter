-- app_client:1234567890

INSERT INTO oauth_client_details
(client_id, client_secret, scope, authorized_grant_types,
 web_server_redirect_uri, authorities, access_token_validity,
 refresh_token_validity, additional_information, autoapprove)
VALUES
('app_client', '$2a$10$GPPhfXgPPUCNupLm7u8DvOLy6o/bxsyJvBVYEbUG0loG068S0Djaa', 'read,write',
 'password,authorization_code,refresh_token', 'http://localhost:8080/oauth/callback', null, 36000, 36000, null, true);