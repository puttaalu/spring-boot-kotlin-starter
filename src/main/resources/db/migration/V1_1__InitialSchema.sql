--
-- Name: schema_version; For flyway
--
CREATE TABLE schema_version
(
    installed_rank integer                                   NOT NULL,
    version        character varying(50),
    description    character varying(200)                    NOT NULL,
    type           character varying(20)                     NOT NULL,
    script         character varying(1000)                   NOT NULL,
    checksum       integer,
    installed_by   character varying(100)                    NOT NULL,
    installed_on   timestamp without time zone DEFAULT now() NOT NULL,
    execution_time integer                                   NOT NULL,
    success        boolean                                   NOT NULL
);


ALTER TABLE ONLY schema_version
    ADD CONSTRAINT schema_version_pk PRIMARY KEY (installed_rank);

-- OAUTH stuff
CREATE TABLE oauth_access_token
(
    token_id          character varying(256),
    token             bytea,
    authentication_id character varying(256),
    user_name         character varying(256),
    client_id         character varying(256),
    authentication    bytea,
    refresh_token     character varying(256)
);


CREATE TABLE oauth_approvals
(
    userid         character varying(255),
    clientid       character varying(255),
    scope          character varying(255),
    status         character varying(10),
    expiresat      timestamp(0) without time zone DEFAULT now(),
    lastmodifiedat timestamp(0) without time zone DEFAULT now()
);


CREATE TABLE oauth_client_details
(
    client_id               character varying(256) NOT NULL,
    resource_ids            character varying(256),
    client_secret           character varying(256),
    scope                   character varying(256),
    authorized_grant_types  character varying(256),
    web_server_redirect_uri character varying(256),
    authorities             character varying(256),
    access_token_validity   integer,
    refresh_token_validity  integer,
    additional_information  character varying(4096),
    autoapprove             character varying(256)
);

CREATE TABLE oauth_client_token
(
    token_id          character varying(256),
    token             bytea,
    authentication_id character varying(256),
    user_name         character varying(256),
    client_id         character varying(256)
);

CREATE TABLE oauth_code
(
    code           character varying(256),
    authentication bytea
);


CREATE TABLE oauth_refresh_token
(
    token_id       character varying(256),
    token          bytea,
    authentication bytea
);

CREATE TABLE clientdetails
(
    appid                  character varying(256) NOT NULL,
    resourceids            character varying(256),
    appsecret              character varying(256),
    scope                  character varying(256),
    granttypes             character varying(256),
    redirecturl            character varying(256),
    authorities            character varying(256),
    access_token_validity  integer,
    refresh_token_validity integer,
    additionalinformation  character varying(4096)
);

CREATE TABLE users
(
    id               uuid                        NOT NULL,
    activation_token character varying(255),
    active           boolean                     NOT NULL,
    auth_token       character varying(255),
    creation_date    timestamp without time zone NOT NULL,
    email_address    character varying(256)      NOT NULL,
    full_name        character varying(255),
    locked           boolean,
    password_hash    character varying(255)      NOT NULL
);

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);

ALTER TABLE ONLY users
    ADD CONSTRAINT users_email_uk UNIQUE (email_address);

CREATE TABLE roles
(
    user_id   uuid                   NOT NULL,
    role_name character varying(128) NOT NULL
);
ALTER TABLE ONLY roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (user_id, role_name);


CREATE INDEX schema_version_s_idx ON schema_version USING btree (success);