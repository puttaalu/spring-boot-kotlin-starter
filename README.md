### Keystore generation

```shell
keytool -genkeypair -alias devKey -keyalg RSA -keypass 123456 -keystore devKeystore.jks -storepass 123456

keytool -list -rfc --keystore devKeystore.jks | openssl x509 -inform pem -pubkey -noout
```